const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

let users = [
  {
    username: "kevs",
    password: "kevs1234",
  },
  {
    username: "dais",
    password: "dais1234",
  },
];


app.get("/home", (request, response) => {
  response.send("this is a test homepage");
});


app.get("/users", (request, response) => {
  response.send(users);
});


app.delete("/delete-user", (request, response) => {
  let message = `User ${request.body.username} has been deleted.`;

  for (let i = 0; i < users.length; i++) {
    if (request.body.username == users[i].username) {
      delete users[i];

      break;
    } else {
      message = "User does not exist";
    }
  }
  response.send(message);
});

app.listen(port, () => console.log(`Server running at port ${port}`));
